#!/bin/bash
# A simple script to disable the touchscreen under XORG
# Created by Christian van Langendonck - Licensed under GPL3
S1=`xinput list | grep Touchscreen | grep pointer | grep -E -o [1-2][0-9]`
xinput disable $S1
