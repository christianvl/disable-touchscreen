# Disable Touchscreen

<p>This is a simple Bash script to disable the monitor touchscreen under Xorg.

## Instructions 

<p>The script works by calling the command `xinput disable x` where x is the id number of the touchscreen.

<p>Best used by calling the script during initialization.

## License
<p> This software is licensed under the [AGPL3](https://www.gnu.org/licenses/agpl-3.0.html) license.
<p> ![AGPL](https://www.gnu.org/graphics/agplv3-88x31.png "AGPL3")
